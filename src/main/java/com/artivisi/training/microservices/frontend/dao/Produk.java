package com.artivisi.training.microservices.frontend.dao;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Produk {
    private String id;
    private String kode;
    private String nama;
    private BigDecimal harga;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public BigDecimal getHarga() {
        return harga;
    }

    public void setHarga(BigDecimal harga) {
        this.harga = harga;
    }
}
